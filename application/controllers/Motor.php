<?php defined('BASEPATH') or exit('No direct script access allowed');

class Motor extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        statusLogin();
    }

    public function index()
    {
        $motors = $this->db->get('motor')->result();

        $data = [ 
            "title"  => "Motor" ,
            "motors" => $motors,
        ];
        
        $this->template->load('layout/master', 'list_motor', $data);
    }

    public function insert_action()
    {
        $nama        = $this->input->post("nama");
        $tipe        = $this->input->post("tipe");
        $spesifikasi = $this->input->post("spesifikasi");
        $hargaFomat  = str_replace('.', '', $this->input->post('harga', TRUE));
		$harga       = str_replace(',', '.', $hargaFomat);

        $data = [
            'nama'        => $nama,
            'tipe'        => $tipe,
            'spesifikasi' => $spesifikasi,
            'harga'       => $harga,
        ];

        if(!empty($_FILES['document']['name'])) {
            $file = $_FILES['document']['name'];
            // config upload
            $config['upload_path']   = FCPATH .'/uploads/';
            $config['allowed_types'] = 'png|jpg|jpeg';
            $config['file_name']     = $file;
            $config['file_size']     = $_FILES['document']['size'] / 1028;
            $config['max_size']      = 2000; // 2MB
            $config['overwrite']     = true;
            
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
    
            if ($this->upload->do_upload('document')) {
                $uploadData = $this->upload->data();
                $data['foto'] = $uploadData['file_name'];
            } else {
                $error_messages = $this->upload->display_errors();
                var_dump($error_messages);die;
            }
        }

        $this->db->insert("motor", $data);
        
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function edit($id)
    {
        $row = $this->db->get_where("motor", ["id" => $id])->row();
        if($row) {
            $data = [
                "title"  => "Motor Form Edit" ,
                'id'   => set_value('id', $row->id),
                'nama' => set_value('nama', $row->nama),
                'tipe' => set_value('tipe', $row->tipe),
                'spesifikasi' => set_value('spesifikasi', $row->spesifikasi),
                'harga' => set_value('harga', rupiah_decimal($row->harga)),
            ];  

            $this->template->load('layout/master', 'form_edit_motor', $data);

        } else {
            redirect($_SERVER['HTTP_REFERER']);
        }
    }

    public function update()
    {
        $id = $this->input->post("id");
        $hargaFomat  = str_replace('.', '', $this->input->post('harga', TRUE));
		$harga       = str_replace(',', '.', $hargaFomat);

        $data = [
            'nama'        => $this->input->post("nama"),
            'tipe'        => $this->input->post("tipe"),
            'spesifikasi' => $this->input->post("spesifikasi"),
            'harga'       => $harga,
        ];
        
        if(!empty($_FILES['document']['name'])) {
            $file = $_FILES['document']['name'];
            // config upload
            $config['upload_path']   = FCPATH .'/uploads/';
            $config['allowed_types'] = 'png|jpg|jpeg';
            $config['file_name']     = $file;
            $config['file_size']     = $_FILES['document']['size'] / 1028;
            $config['max_size']      = 2000; // 2MB
            $config['overwrite']     = true;
            
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
    
            if ($this->upload->do_upload('document')) {

                $uploadData = $this->upload->data();
                $data['foto'] = $uploadData['file_name'];
            } else {
                $error_messages = $this->upload->display_errors();
                var_dump($error_messages);die;
            }
        }

        $this->db->where('id', $id);
        $this->db->update("motor", $data);

        redirect(site_url('motor'));
    }

    public function delete($id)
    {
        $row = $this->db->get_where("motor", ["id" => $id])->row();
        if($row) {
            $this->db->where('id', $id);
            $this->db->delete('motor');

            redirect($_SERVER['HTTP_REFERER']);
        } else {
            redirect($_SERVER['HTTP_REFERER']);
        }
    }
}
