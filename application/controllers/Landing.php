<?php defined('BASEPATH') or exit('No direct script access allowed');

class Landing extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $profil  = $this->db->get('profil')->row();
        $jamBuka = $this->db->get('jam_buka')->result();
        $motors  = $this->db->order_by('id', 'desc')->limit(6)->get('motor')->result();
        
        $data = [ 
            "title"    => "Landing Page",
            "profil"   => $profil,
            "jamBuka"  => $jamBuka,
            "motors"   => $motors,
        ];
        $this->template->load('landing/master', 'landing/v_landing', $data);
    }

    public function about()
    {
        $profil  = $this->db->get('profil')->row();
        $jamBuka = $this->db->get('jam_buka')->result();
        
        $data = [ 
            "title"    => "About Page",
            "profil"   => $profil,
            "jamBuka"  => $jamBuka,
        ];

        $this->template->load('landing/master', 'landing/v_about', $data);
    }

    public function contact()
    {
        $profil  = $this->db->get('profil')->row();
        $jamBuka = $this->db->get('jam_buka')->result();
        
        $data = [ 
            "title"    => "Contact Page",
            "profil"   => $profil,
            "jamBuka"  => $jamBuka,
        ];

        $this->template->load('landing/master', 'landing/v_contact', $data);
    }

    public function motor()
    {
        $q = strtolower($this->input->get('q'));
        if(isset($q)) {
            $motors = $this->db->query("select * from motor where lower(nama) like '%$q%'")->result();
        } else {
            $motors = $this->db->get('motor')->result();
        }

        $filter = $this->input->get('filter');
        if(isset($filter)) {
            $motors = $this->db->query("select * from motor where tipe='$filter'")->result();
        } else {
            $motors = $this->db->get('motor')->result();
        }

        $profil  = $this->db->get('profil')->row();
        $jamBuka = $this->db->get('jam_buka')->result();

        $data = [ 
            "title"    => "Motor Page",
            "profil"   => $profil,
            "jamBuka"  => $jamBuka,
            "motors"   => $motors,
        ];

        $this->template->load('landing/master', 'landing/v_motor', $data);
    }

    public function singleMotor($id)
    {
        $profil  = $this->db->get('profil')->row();
        $jamBuka = $this->db->get('jam_buka')->result();
        $motor   = $this->db->get_where('motor', ["id" => $id])->row();

        $data = [ 
            "title"    => "Single Motor Page",
            "profil"   => $profil,
            "jamBuka"  => $jamBuka,
            "motor"    => $motor
        ];

        $this->template->load('landing/master', 'landing/v_single_motor', $data);
    }

    public function sendPesan()
    {
        $name        = $this->input->post("name");
        $no_whatsapp = $this->input->post("no_whatsapp");
        $subject     = $this->input->post("subject");
        $message     = $this->input->post("message");

        $data = [
            'name'        => $name,
            'no_whatsapp' => $no_whatsapp,
            'subject'     => $subject,
            'message'     => $message,
            'create_date' => date("Y-m-d H:i:s")
        ];
        $this->db->insert("pesan", $data);

        $this->session->set_flashdata("success", "Berhasil kirim pesan.");
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function login()
    {
        $profil  = $this->db->get('profil')->row();
        $jamBuka = $this->db->get('jam_buka')->result();

        $data = [
            'title' => 'Login',
            "profil"   => $profil,
            "jamBuka"  => $jamBuka,
        ];

        $this->template->load('landing/master', 'landing/v_login', $data);
    }

    public function loginPelanggan()
    {
        $username = $this->input->post("username", TRUE);
        $password = md5($this->input->post("password", TRUE));
        
        $getUser = $this->db->get_where("user", ["username" => $username])->row();
        $checkUsername = !empty($getUser) ? $getUser->username : '';
        $checkPassword = !empty($getUser) ? $getUser->password : '';

        if ($username == $checkUsername) {
            if($password == $checkPassword) {
                $dataSession = array(
                    'status'   => "user_pelanggan",
                    'username' => $checkUsername,
                    'user_id'  => $getUser->id,
                );		
                
                $this->session->set_userdata($dataSession);
                redirect(site_url("landing"));

            } else {
                $this->session->set_flashdata('error-alert', 'Password salah.');
                $this->login();
            }
            
        } else {
            $this->session->set_flashdata('error-alert', 'Username tidak ditemukan.');
            $this->login();
        }
    }

    public function logoutPelanggan()
    {
        $this->session->sess_destroy();
		redirect(base_url());
    }

    public function detailPelanggan()
    {
        $profil  = $this->db->get('profil')->row();
        $jamBuka = $this->db->get('jam_buka')->result();

        $user_id = $this->session->user_id;
        $getUser = $this->db->get_where("user", ["id" => $user_id])->row();
        $transaksis = $this->db->order_by("tanggal_pemesanan", "desc")->get_where("transaksi", ["user_id" => $user_id])->result();
        
        $data = [
            'title'    => 'Detail Pelanggan',
            "profil"   => $profil,
            "jamBuka"  => $jamBuka,
            "user"     => $getUser ,
            "transaksis" => $transaksis,
        ];
        $this->template->load('landing/master', 'landing/v_detail_pelanggan', $data);
    }

    public function beliMotor($motor_id)
    {
        $profil  = $this->db->get('profil')->row();
        $jamBuka = $this->db->get('jam_buka')->result();

        $user_id = $this->session->user_id;
        if($user_id) {
            $getUser = $this->db->get_where("user", ["id" => $user_id])->row();
            $motor   = $this->db->get_where('motor', ["id" => $motor_id])->row();
            
            $data = [
                'title'    => 'Beli Motor',
                "profil"   => $profil,
                "jamBuka"  => $jamBuka,
                "user"     => $getUser,
                "motor"    => $motor
            ];
            $this->template->load('landing/master', 'landing/v_beli_motor', $data);
        } else {
            $this->session->set_flashdata("error", "Silahkan login terlebih dahulu.");
            redirect($_SERVER['HTTP_REFERER']);
        }
    }

    public function generate_kode()
    {
        date_default_timezone_set('Asia/Jakarta');

        $tgl = date('ymd');
        $query = $this->db->query("select MAX(RIGHT(a.no_invoice,6)) as kode_sekarang from transaksi a where a.no_invoice like 'LBD-$tgl%'");
        $tgl = date('ymd');
        if ($query->num_rows() <> 0) {
            //cek kode jika telah tersedia 
            $kode = ($query->row()->kode_sekarang) + 1;
        } else {
            $kode = 1;  //cek jika kode belum terdapat pada table
        }
        $batas = str_pad($kode, 6, "0", STR_PAD_LEFT);
        $kodetampil = "LBD-" . $tgl . $batas;  //format kode
        return $kodetampil;
    }

    public function checkoutMotor()
    {
        $user_id            = $this->input->post("user_id");
        $motor_id           = $this->input->post("motor_id");
        $harga              = $this->input->post("harga");
        $tanggal_pemesanan  = date("Y-m-d H:i:s");
        $status             = "menunggu";
        $no_invoice         = $this->generate_kode();

        $data = [
            'user_id'           => $user_id,
            'motor_id'          => $motor_id,
            'harga'             => $harga,
            'tanggal_pemesanan' => $tanggal_pemesanan,
            'status'            => $status,
            'no_invoice'        => $no_invoice,
        ];
        $this->db->insert("transaksi", $data);

        $this->session->set_flashdata("success", "Berhasil checkout.");
        redirect(site_url("landing/detailPelanggan"));
    }

    public function batalPemesanan($id)
    {
        $transaksi = $this->db->get_where("transaksi", ["id" => $id])->row();
        if($transaksi) {
            $this->db->where("id", $id);
            $this->db->delete("transaksi");

            $this->session->set_flashdata("success", "Berhasil batal pemesanan.");
            redirect(site_url("landing/detailPelanggan"));
        } else {
            $this->session->set_flashdata("error", "Gagal.");
            redirect(site_url("landing/detailPelanggan"));
        }
    }

    public function konfirmasiPembayaran($id)
    {
        $transaksi = $this->db->get_where("transaksi", ["id" => $id])->row();
        if($transaksi) {
            $data = [
                'status'         => "sudah dibayar",
                'bukti_transfer' => "Konfirmasi Pembayaran Via WA",
            ];

            $this->db->where("id", $id);
            $this->db->update("transaksi", $data);

            $this->session->set_flashdata("success", "Berhasil konfirmasi pemesanan.");
            redirect(site_url("landing/detailPelanggan"));
        } else {
            $this->session->set_flashdata("error", "Gagal.");
            redirect(site_url("landing/detailPelanggan"));
        }
    }

    public function cetakInvoice($id)
    {
        $transaksi = $this->db->get_where("transaksi", ["id" => $id])->row();
        if($transaksi) {
            $data = [
                'transaksi' => $transaksi,
            ];

            $this->load->view('cetak_invoice', $data);
        } else {
            $this->session->set_flashdata("error", "Gagal.");
            redirect(site_url("landing/detailPelanggan"));
        }
    }

    public function register()
    {
        $profil  = $this->db->get('profil')->row();
        $jamBuka = $this->db->get('jam_buka')->result();

        $data = [
            'title'    => 'Register',
            "profil"   => $profil,
            "jamBuka"  => $jamBuka,
            "nama"     => set_value("nama"),
            "no_hp"    => set_value("no_hp"),
            "alamat"   => set_value("alamat"),
            "username" => set_value("username"),
        ];

        $this->template->load('landing/master', 'landing/v_register', $data);
    }

    public function registerAkun()
    {
        $nama      = $this->input->post("nama");
        $no_hp     = $this->input->post("no_hp");
        $alamat    = $this->input->post("alamat");
        $username  = $this->input->post("username");
        $password  = md5($this->input->post("password"));

        $data = [
            'nama'     => $nama,
            'no_hp'    => $no_hp,
            'alamat'   => $alamat,
            'username' => $username,
            'password' => $password,
        ];
        $this->db->insert("user", $data);

        $this->session->set_flashdata("success", "Berhasil membuat akun, silahkan login.");
        redirect(site_url("landing/login"));
    }
}
