<?php defined('BASEPATH') or exit('No direct script access allowed');

class Profil extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        statusLogin();
    }

    public function index()
    {
        $profil  = $this->db->get('profil')->row();
        $jamBuka = $this->db->get('jam_buka')->result();

        $data = [ 
            "title"        => "Profil" ,
            "id"           => !empty($profil) ? $profil->id : "",
            "tentang_kami" => !empty($profil) ? $profil->tentang_kami : "",
            "whatsapp"     => !empty($profil) ? $profil->whatsapp : "",
            "no_telpon"    => !empty($profil) ? $profil->no_telpon : "",
            "email"        => !empty($profil) ? $profil->email : "",
            "langitude"    => !empty($profil) ? $profil->langitude : "",
            "longitude"    => !empty($profil) ? $profil->longitude : "",
            "jamBuka"      => $jamBuka,
        ];
        
        $this->template->load('layout/master', 'form_profil', $data);
    }

    public function storeAction()
    {
        $id           = $this->input->post("id");
        $tentang_kami = $this->input->post("tentang_kami");
        $whatsapp     = $this->input->post("whatsapp");
        $no_telpon    = $this->input->post("no_telpon");
        $email        = $this->input->post("email");
        $langitude    = $this->input->post("langitude");
        $longitude    = $this->input->post("longitude");

        $data = [
            'tentang_kami' => $tentang_kami,
            'whatsapp'     => $whatsapp,
            'no_telpon'    => $no_telpon,
            'email'        => $email,
            'langitude'    => $langitude,
            'longitude'    => $longitude,
        ];

        if(empty($id)) {
            $this->db->insert('profil', $data);
        } else {
            $this->db->where('id', $id);
            $this->db->update('profil', $data);
        }

        redirect($_SERVER['HTTP_REFERER']);
    }

    public function updateJamBuka()
    {
        $id   = $this->input->post("id");
        $buka = $this->input->post("buka");

        $data = [
            'buka' => $buka
        ];

        $row = $this->db->get_where("jam_buka", ["id" => $id])->row();
        if($row) {
            $this->db->where("id", $id);
            $this->db->update("jam_buka", $data);

            echo json_encode([
                'status' => 'success'
            ]);

        } else {
            echo json_encode([
                'status' => 'error'
            ]);
        }
    }

    public function updateJamTutup()
    {
        $id    = $this->input->post("id");
        $tutup = $this->input->post("tutup");

        $data = [
            'tutup' => $tutup
        ];

        $row = $this->db->get_where("jam_buka", ["id" => $id])->row();
        if($row) {
            $this->db->where("id", $id);
            $this->db->update("jam_buka", $data);

            echo json_encode([
                'status' => 'success'
            ]);

        } else {
            echo json_encode([
                'status' => 'error'
            ]);
        }
    }
}
