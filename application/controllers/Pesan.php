<?php defined('BASEPATH') or exit('No direct script access allowed');

class Pesan extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        statusLogin();
    }

    public function index()
    {
        $pesans = $this->db->order_by("create_date", "desc")->get('pesan')->result();

        $data = [ 
            "title"  => "Pesan" ,
            "pesans" => $pesans,
        ];
        
        $this->template->load('layout/master', 'list_pesan', $data);
    }
}
