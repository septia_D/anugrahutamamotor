<?php defined('BASEPATH') or exit('No direct script access allowed');

class Transaksi extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        statusLogin();
    }

    public function index()
    {
        $transaksis = $this->db->order_by("tanggal_pemesanan", "desc")->get('transaksi')->result();

        $data = [ 
            "title"      => "Transaksi" ,
            "transaksis" => $transaksis,
        ];
        
        $this->template->load('layout/master', 'list_transaksi', $data);
    }

    public function updateStatus($id)
    {
        $transaksi = $this->db->get('transaksi', ["id" => $id])->row();
        if($transaksi) {
            $data = [
                "title"     => "Transaksi Form Edit" ,
                "transaksi" => $transaksi
            ];  

            $this->template->load('layout/master', 'form_edit_transaksi', $data);

        } else {
            redirect($_SERVER['HTTP_REFERER']);
        }
    }

    public function updateStatusAction()
    {
        $id     = $this->input->post("id");
        $status = $this->input->post("status");

        $data = [
            'status' => $status,
        ];

        $this->db->where("id", $id);
        $this->db->update("transaksi", $data);

        $this->session->set_flashdata("success", "Berhasil update status.");
        redirect(site_url("transaksi"));
    }

    public function invoice($id)
    {
        $transaksi = $this->db->get_where("transaksi", ["id" => $id])->row();
        if($transaksi) {
            $data = [
                'transaksi' => $transaksi,
            ];

            $this->load->view('cetak_invoice', $data);
        } else {
            $this->session->set_flashdata("error", "Gagal.");
            redirect(site_url("landing/detailPelanggan"));
        }
    }
}
