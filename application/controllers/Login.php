<?php defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('bcrypt');
    }

    public function index()
    {
        $data = [ 
            "title"    => "Log In",
            "username" => set_value("username")
        ];
        $this->load->view('v_login', $data);
    }

    public function authentication()
    {
        $username = $this->input->post("username", TRUE);
        $password = md5($this->input->post("password", TRUE));

        $checkUsername = 'admin';
        $checkPassword = md5('@dmin2024');

        if ($username == $checkUsername) {
            if($password == $checkPassword) {
                $dataSession = array(
                    'status' => "login",
                );		
                
                $this->session->set_userdata($dataSession);
                redirect(site_url("dashboard"));

            } else {
                $this->session->set_flashdata('error-alert', 'Password salah.');
                $this->index();
            }
            
        } else {
            $this->session->set_flashdata('error-alert', 'Username tidak ditemukan.');
            $this->index();
        }
    }

    public function sessionDestroy()
    {
        $this->session->sess_destroy();
		redirect(base_url());
    }
}
