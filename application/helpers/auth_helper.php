<?php

function statusLogin() 
{
	$CI = &get_instance();

	if($CI->session->status != "login"){
		$CI->session->set_flashdata("error", "Silahkan login terlebih dahulu");
		redirect(base_url("/"));
	}
}

function rp_rupiah_decimal($angka){
	$hasil_rupiah = number_format($angka,2,',','.');
	return "Rp ".$hasil_rupiah;
}

function rupiah_decimal($angka){
	$hasil_rupiah = number_format($angka,0,',','.');
	return $hasil_rupiah;
}

