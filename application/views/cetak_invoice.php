<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title><?= $transaksi->no_invoice ?></title>

	<style>
		#show {
			display: block;
		}

		#hidden {
			display: none;
			font-size: 14px;
		}

		* {
			box-sizing: border-box;
		}

		.judul {
			font-family: Arial, sans-serif;
			font-size: 16px;
		}

		.tg {
			border: none;
			margin-top: 10px
		}

		.tg td {
			font-family: Arial, sans-serif;
			font-size: 12px;
		}

		.tg2 {
			border-collapse: collapse;
			border-spacing: 0;
			width: 100%;
			margin-top: 10px
		}

		.tg2 td {
			font-family: Arial, sans-serif;
			font-size: 12px;
			padding: 5px 5px;
			overflow: hidden;
			word-break: normal;
			vertical-align: top;
		}

		.tg2 th {
			font-family: Arial, sans-serif;
			font-size: 12px;
			padding: 5px 5px;
			border-width: 1px;
			overflow: hidden;
			word-break: normal;
		}

		.tg2 .judultabel {
			font-weight: bold;
			text-align: center;
		}

		.column {
			float: left;
			width: 25%;
			padding: 1px;
		}

		.column2 {
			float: left;
			width: 50%;
			padding: 1px;
		}

		.column3 {
			float: left;
			width: 45%;
			padding: 1px;
		}

		.row:after {
			content: "";
			display: table;
			clear: both;
		}

		p {
			font-family: Arial, sans-serif;
			font-size: 12px;
		}

		@media print {
			@page {
				/*size: 330mm 215mm;*/
				size: 'A4';
				size: portrait;
				margin: 30px;
				column-count: 2;
				border: 3px solid black;
			}

			p {
				line-height: 1.7em;
				margin: 0;
			}

			#show {
				display: none;
			}

			#hidden {
				display: block;
			}

			.col-sm-1,
			.col-sm-2,
			.col-sm-3,
			.col-sm-4,
			.col-sm-5,
			.col-sm-6,
			.col-sm-7,
			.col-sm-8,
			.col-sm-9,
			.col-sm-10,
			.col-sm-11,
			.col-sm-12 {
				float: left;
			}

			.col-sm-12 {
				width: 100%;
			}

			.col-sm-11 {
				width: 91.66666667%;
			}

			.col-sm-10 {
				width: 83.33333333%;
			}

			.col-sm-9 {
				width: 75%;
			}

			.col-sm-8 {
				width: 66.66666667%;
			}

			.col-sm-7 {
				width: 58.33333333%;
			}

			.col-sm-6 {
				width: 50%;
			}

			.col-sm-5 {
				width: 41.66666667%;
			}

			.col-sm-4 {
				width: 33.33333333%;
			}

			.col-sm-3 {
				width: 25%;
			}

			.col-sm-2 {
				width: 16.66666667%;
			}

			.col-sm-1 {
				width: 8.33333333%;
			}

            #customers {
				font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
				border-collapse: collapse;
				width: 100%;
			}

			#customers td,
			#customers th {
				border: 1px solid;
				padding-left: 4px;
				vertical-align: top;
				text-align: center;
			}

			#customers tr:hover {
				background-color: #ddd;
			}

			#customers th {
				text-align: center;
			}

		}
	</style>
</head>

<body onload="setTimeout(cetak,1000)">
	<div id="show">Loading...</div>
	<div id="hidden">
		<div class="row">
			<div class="col-sm-12" style="text-align: center">
				<div class="judul"><b>Invoice Anugrah Utama Motor</b></div>
			</div>
		</div>
        <br>
        <br>
		<div class="row">
            <table class="" id="tg2">
                <tr>
                    <td width="20%" style="text-align: left">No. Invoice</td>
                    <td width="1%">:</td>
                    <td style="text-align: left"><?= $transaksi->no_invoice ?></td>
                </tr>
                <tr>
                    <td width="20%" style="text-align: left">Tanggal Pemesanan</td>
                    <td width="1%">:</td>
                    <td style="text-align: left"><?= $transaksi->tanggal_pemesanan ?></td>
                </tr>
                <tr>
                    <td width="20%" style="text-align: left">Nama Pembeli</td>
                    <td width="1%">:</td>
                    <td style="text-align: left">
                        <?php  
                            $user = $this->db->get_where("user", ["id" => $transaksi->user_id])->row();
                            echo ucwords($user->nama);
                        ?>
                    </td>
                </tr>
                <tr>
                    <td width="20%" style="text-align: left">No. HP</td>
                    <td width="1%">:</td>
                    <td style="text-align: left"><?= $user->no_hp ?></td>
                </tr>
                <tr>
                    <td width="20%" style="text-align: left">Motor</td>
                    <td width="1%">:</td>
                    <td style="text-align: left">
                        <?php 
                            $motor = $this->db->get_where("motor", ["id" => $transaksi->motor_id])->row();
                            echo $motor->nama;
                        ?>
                    </td>
                </tr>
                <tr>
                    <td width="20%" style="text-align: left">Harga</td>
                    <td width="1%">:</td>
                    <td style="text-align: left"><?= rp_rupiah_decimal($transaksi->harga) ?></td>
                </tr>
                <tr>
                    <td width="20%" style="text-align: left">Status</td>
                    <td width="1%">:</td>
                    <td style="text-align: left"><b><?= strtoupper($transaksi->status) ?></b></td>
                </tr>
            </table>
		</div>
	</div>
</body>
<script type="text/javascript">
	function cetak() {
		print();
	}
</script>
<meta http-equiv="refresh" content="2; <?= $_SERVER['HTTP_REFERER']; ?>">
</body>

</html>
