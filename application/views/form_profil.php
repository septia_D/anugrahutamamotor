<!--begin::Content wrapper-->
<div class="d-flex flex-column flex-column-fluid">
    <!--begin::Toolbar-->
    <div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
        <!--begin::Toolbar container-->
        <div id="kt_app_toolbar_container" class="app-container">
            <!--begin::Page title-->
            <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
                <!--begin::Title-->
                <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">Profil Form</h1>
                <!--end::Title-->
                <!--begin::Breadcrumb-->
                <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                    <!--begin::Item-->
                    <li class="breadcrumb-item text-muted">
                        <a href="#" class="text-muted text-hover-primary">Home</a>
                    </li>
                    <!--end::Item-->
                    <!--begin::Item-->
                    <li class="breadcrumb-item">
                        <span class="bullet bg-gray-400 w-5px h-2px"></span>
                    </li>
                    <!--end::Item-->
                    <!--begin::Item-->
                    <li class="breadcrumb-item text-muted">Profil</li>
                    <!--end::Item-->
                </ul>
                <!--end::Breadcrumb-->
            </div>
            <!--end::Page title-->
        </div>
        <!--end::Toolbar container-->
    </div>
    <!--end::Toolbar-->
    <!--begin::Form-->
    <form id="profil_form" class="form" action="<?= site_url('profil/storeAction') ?>" method="post">
        <input type="hidden" name="id" value="<?= $id ?>">
        <!--end::Input-->
        <!--begin::Content-->
        <div id="kt_app_content" class="app-content flex-column-fluid">
            <!--begin::Content container-->
            <div id="kt_app_content_container" class="app-container">
                <!--begin::form hps-->
                <div class="row g-7 mb-5">
                    <!--begin::Content-->
                    <div class="col-xl-12">
                        <!--begin::Contacts-->
                        <div class="card card-flush h-lg-100" id="kt_hps_main">
                            <!--begin::Card body-->
                            <div class="card-body pt-5">
                                <!--begin::Input group-->
                                <div class="row mb-2">
                                    <!--begin::Label-->
                                    <label class="fs-6 fw-semibold form-label mt-3">
                                        <span class="required">Tentang Kami</span>
                                        <i class="fas fa-exclamation-circle ms-1 fs-7" data-bs-toggle="tooltip" title="Masukkan tentang kami jika ada."></i>
                                    </label>
                                    <div class="col-md-12">
                                        <textarea name="tentang_kami" id="kt_docs_ckeditor_classic"><?= $tentang_kami ?></textarea>
                                    </div>
                                </div>
                                <!--end::Input group-->
                                <div class="separator separator-content border-dark my-15"><span class="w-250px fw-bold">Kontak Kami</span></div>
                                <!--begin::Input group-->
                                <div class="row mb-2">
                                    <!--begin::Label-->
                                    <label class="fs-6 fw-semibold form-label mt-3">
                                        <span class="required">Whatsapp</span>
                                        <i class="fas fa-exclamation-circle ms-1 fs-7" data-bs-toggle="tooltip" title="Masukkan no whatsapp jika ada."></i>
                                    </label>
                                    <!--begin::Input-->
                                    <input type="text" name="whatsapp" class="form-control" id="whatsapp" value="<?= $whatsapp ?>" autocomplete="off"/>
                                    <!--end::Input-->
                                </div>
                                <!--end::Input group-->
                                <!--begin::Input group-->
                                <div class="row mb-2">
                                    <!--begin::Label-->
                                    <label class="fs-6 fw-semibold form-label mt-3">
                                        <span class="required">No. Telpon</span>
                                        <i class="fas fa-exclamation-circle ms-1 fs-7" data-bs-toggle="tooltip" title="Masukkan no. telpon jika ada."></i>
                                    </label>
                                    <!--begin::Input-->
                                    <input type="text" name="no_telpon" class="form-control" id="no_telpon" value="<?= $no_telpon ?>" autocomplete="off"/>
                                    <!--end::Input-->
                                </div>
                                <!--end::Input group-->
                                <!--begin::Input group-->
                                <div class="row mb-2">
                                    <!--begin::Label-->
                                    <label class="fs-6 fw-semibold form-label mt-3">
                                        <span class="required">Email</span>
                                        <i class="fas fa-exclamation-circle ms-1 fs-7" data-bs-toggle="tooltip" title="Masukkan email jika ada."></i>
                                    </label>
                                    <!--begin::Input-->
                                    <input type="text" name="email" class="form-control" id="email" value="<?= $email ?>" autocomplete="off"/>
                                    <!--end::Input-->
                                </div>
                                <!--end::Input group-->
                                <div class="separator separator-content border-dark my-15"><span class="w-250px fw-bold">Peta</span></div>
                                <!--begin::Input group-->
                                <div class="row mb-2">
                                    <!--begin::Label-->
                                    <label class="fs-6 fw-semibold form-label mt-3">
                                        <span class="required">Langitude</span>
                                        <i class="fas fa-exclamation-circle ms-1 fs-7" data-bs-toggle="tooltip" title="Masukkan langitude jika ada."></i>
                                    </label>
                                    <!--begin::Input-->
                                    <input type="text" name="langitude" class="form-control" id="langitude" value="<?= $langitude ?>" autocomplete="off"/>
                                    <!--end::Input-->
                                </div>
                                <!--end::Input group-->
                                <!--begin::Input group-->
                                <div class="row mb-5">
                                    <!--begin::Label-->
                                    <label class="fs-6 fw-semibold form-label mt-3">
                                        <span class="required">Longitude</span>
                                        <i class="fas fa-exclamation-circle ms-1 fs-7" data-bs-toggle="tooltip" title="Masukkan longitude jika ada."></i>
                                    </label>
                                    <!--begin::Input-->
                                    <input type="text" name="longitude" class="form-control" id="longitude" value="<?= $longitude ?>" autocomplete="off"/>
                                    <!--end::Input-->
                                </div>
                                <!--end::Input group-->
                                <div class="separator separator-content border-dark my-15"><span class="w-250px fw-bold">Jam Buka</span></div>
                                <div class="row mb-2">
                                    <div class="table-responsive">
                                        <table class="table table-bordered" id="tableJadwal">
                                            <thead>
                                                <tr class="fw-bold fs-6 text-gray-800">
                                                    <th>Hari</th>
                                                    <th>Buka</th>
                                                    <th>Tutup</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach($jamBuka as $jb) : ?>
                                                    <tr>
                                                        <td><?= $jb->hari ?></td>
                                                        <td>
                                                            <input type="time" name="buka" class="form-control buka" value="<?= $jb->buka ?>" data-id="<?= $jb->id ?>" autocomplete="off"/>
                                                        </td>
                                                        <td>
                                                            <input type="time" name="tutup" class="form-control tutup" value="<?= $jb->tutup ?>" data-id="<?= $jb->id ?>" autocomplete="off"/>
                                                        </td>
                                                    </tr>
                                                <?php endforeach; ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!--end::Card body-->
                        </div>
                        <!--end::Contacts-->
                    </div>
                    <!--end::Content-->

                    
                </div>

                <div class="d-flex justify-content-end">
                    <button type="submit" id="kt_add_hps_submit" class="btn btn-primary me-5">
                        <span class="indicator-label">Simpan</span>
                        <span class="indicator-progress">Please wait...
                        <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                    </button>
                </div>
            </div>
        </div>
    </form>
    <!--end::Form-->
</div>

<!--begin:Javascript -->
<?php $this->load->view("layout/extends-js") ?>
<!--end::Javascript-->
<!--CKEditor Build Bundles:: Only include the relevant bundles accordingly-->
<script src="<?= base_url('assets/template/plugins/custom/ckeditor/ckeditor-classic.bundle.js') ?>"></script>
<script>
    ClassicEditor
    .create(document.querySelector('#kt_docs_ckeditor_classic'))
    .then(editor => {
        console.log(editor);
    })
    .catch(error => {
        console.error(error);
    });

    $(document).ready(function() {
        $('#tableJadwal').on('change', '.buka', function(e) {
            let buka = $(this).val();
            let id   = $(this).data('id');

            $.ajax({
                statusCode: {
                    404: function() {
                        Swal.fire({
                            title: "Error",
                            text: 'page not found',
                            type: "error",
                            showConfirmButton: true,
                        })
                    },
                    500: function() {
                        Swal.fire({
                            title: "Error",
                            text: 'internal service error (500)',
                            type: "error",
                            showConfirmButton: true,
                        })
                    }
                },
                data: {
                    id: id,
                    buka: buka,
                },
                type: "POST",
                dataType: "JSON",
                url: "<?= site_url('Profil/updateJamBuka') ?>",
                success: function(data) {
                    if(data.status == 'success') {
                        toastSuccess();
                    } else {
                        toastError();
                    }
                },
                error: function(data) {}
            })
        });

        $('#tableJadwal').on('change', '.tutup', function(e) {
            let tutup = $(this).val();
            let id   = $(this).data('id');

            $.ajax({
                statusCode: {
                    404: function() {
                        Swal.fire({
                            title: "Error",
                            text: 'page not found',
                            type: "error",
                            showConfirmButton: true,
                        })
                    },
                    500: function() {
                        Swal.fire({
                            title: "Error",
                            text: 'internal service error (500)',
                            type: "error",
                            showConfirmButton: true,
                        })
                    }
                },
                data: {
                    id: id,
                    tutup: tutup,
                },
                type: "POST",
                dataType: "JSON",
                url: "<?= site_url('Profil/updateJamTutup') ?>",
                success: function(data) {
                    if(data.status == 'success') {
                        toastSuccess();
                    } else {
                        toastError();
                    }
                },
                error: function(data) {}
            })
        });
    });
</script>
