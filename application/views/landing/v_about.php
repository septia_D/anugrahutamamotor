    <section class="bg-success py-5">
        <div class="container">
            <div class="row align-items-center py-5">
                <div class="col-md-8 text-white">
                    <h1>Tentang Kami</h1>
                    <?= $profil->tentang_kami ?>
                </div>
                <div class="col-md-4">
                    <img src="<?= base_url('uploads/logo.jpeg') ?>" alt="LOGO" width="70%">
                </div>
            </div>
        </div>
    </section>
    <!-- Close Banner -->