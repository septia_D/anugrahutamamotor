<style>
    .teks_besar {
        font-size: 70px;
    }

    /* membuat minimal tinggi satu elemen terutama contain di tab pane */
    .minimal_tinggi {
        min-height: 450px;
    }

    /* membuat background abu abu di tab pane bagian header */
    .bg_abu_abu {
        background-color: #ddd;
    }

    /* menyamakan lebar addon jadi 50 px */
    .input-group-addon {
        min-width: 50px;
    }

    /* pengganti formcontrol jika tidak berfungsi di kondisi tertentu */
    .lebar_peuh {
        width: 100%;
    }

    /* membuat border 1px tipis di tab pane  */
    .border_tipis {
        border: 1px solid #ddd;
    }

    /* meminimalkan margin  */
    .minimal_margin {
        margin: 0px;
    }

    /* menyempitkan tabel pembungkus form pasien */
    .minimal_padding>tbody>tr>td,
    .minimal_padding {
        padding: 3px;
    }

    /* PENTNG memberi tanda hijau di span */
    .terisi {
        background-color: #00a65a !important;
        color: #ddd !important;
    }

    /* PENTNG memberi tanda merah di span */
    .kosong {
        background-color: #d73925 !important;
        color: #ddd !important;
    }
</style>
<section class="">
    <div class="container py-5">
        <div class="row py-3">
            <div class="col-lg-12 m-auto">
                <h1 class="h1 text-center">Profil</h1>      
                <table class="table table-striped table-bordered minimal_margin minimal_padding">
                    <tr>
                        <th width="15%">Nama</th>
                        <td width="1%">:</td>
                        <td>
                            <?= !empty($user) ? $user->nama : '' ?>
                        </td>
                    </tr>
                    <tr>
                        <th>Username</th>
                        <td>:</td>
                        <td>
                            <?= !empty($user) ? $user->username : '' ?>
                        </td>
                    </tr>
                    <tr>
                        <th>No. HP</th>
                        <td>:</td>
                        <td>
                            <?= !empty($user) ? $user->no_hp : '' ?>
                        </td>
                    </tr>
                    <tr>
                        <th>Alamat</th>
                        <td>:</td>
                        <td>
                            <?= !empty($user) ? $user->alamat : '' ?>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="row py-3">
            <div class="col-lg-12 m-auto">
                <h1 class="h1 text-center">Riwayat Pemesanan</h1>
                <div class="list-group">
                    <a href="#" class="list-group-item list-group-item-action active">
                        Tata Cara Pembayaran
                    </a>
                    <a href="#" class="list-group-item list-group-item-action">1. Transfer ke Rekening Mandiri Anugrah Utama Motor 1170010534782.</a>
                    <li href="#" class="list-group-item list-group-item-action">
                        2. Konfirmasi pembayaran melalui Whatsapp 
                        <a class="text-black" href="https://api.whatsapp.com/send?phone=<?= !empty($profil) ? $profil->whatsapp : '' ?>"><?= !empty($profil) ? $profil->whatsapp : '-' ?></a>
                    </li>
                    <a href="#" class="list-group-item list-group-item-action">3. Klik Tombol sudah kirim bukti pembayaran di riwayat pesanan.</a>
                </div>      
                <table class="table table-striped table-bordered minimal_margin minimal_padding mt-5">
                    <thead>
                        <tr>
                            <th style="text-align: center" width="3%">No.</th>
                            <th style="text-align: center">Tanggal Pemesanan</th>
                            <th style="text-align: center">Motor</th>
                            <th style="text-align: center">Status</th>
                            <th style="text-align: center" width="40%">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($transaksis as $key => $t) : ?>
                            <tr>
                                <td style="text-align: center"><?= ++$key ?></td>
                                <td style="text-align: center"><?= $t->tanggal_pemesanan ?></td>
                                <td>
                                    <?php 
                                        $motor = $this->db->get_where("motor", ["id" => $t->motor_id])->row();
                                        echo $motor->nama;
                                    ?>
                                </td>
                                <td style="text-align: center">
                                    <b><?= $t->status ?></b>
                                </td>
                                <td style="text-align: center">
                                    <?php if($t->status == "menunggu") : ?>
                                        <a href="<?= site_url("landing/konfirmasiPembayaran/". $t->id) ?>" class="btn btn-primary">Sudah Kirim Bukti Pembayaran Via WA</a>
                                        <a href="<?= site_url("landing/batalPemesanan/". $t->id) ?>" class="btn btn-danger">Batal</a>
                                    <?php endif; ?>
                                    <a href="<?= site_url("landing/cetakInvoice/". $t->id) ?>" class="btn btn-info">Invoice</a>
                                    
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>