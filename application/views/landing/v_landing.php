    <!-- Start Banner Hero -->
    <div id="template-mo-zay-hero-carousel" class="carousel slide" data-bs-ride="carousel">
        <ol class="carousel-indicators">
            <li data-bs-target="#template-mo-zay-hero-carousel" data-bs-slide-to="0" class="active"></li>
            <li data-bs-target="#template-mo-zay-hero-carousel" data-bs-slide-to="1"></li>
        </ol>
        <div class="carousel-inner">
            <div class="carousel-item active">
                <div class="container">
                    <div class="row p-5">
                        <div class="mx-auto col-md-12 col-lg-12 order-lg-last">
                            <img class="img-fluid" src="<?= base_url('assets/images/landing/1.jpg') ?>" alt="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="carousel-item">
                <div class="container">
                    <div class="row p-5">
                        <div class="mx-auto col-md-12 col-lg-12 order-lg-last">
                            <img class="img-fluid" src="<?= base_url('assets/images/landing/2.jpg') ?>" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <a class="carousel-control-prev text-decoration-none w-auto ps-3" href="#template-mo-zay-hero-carousel" role="button" data-bs-slide="prev">
            <i class="fas fa-chevron-left"></i>
        </a>
        <a class="carousel-control-next text-decoration-none w-auto pe-3" href="#template-mo-zay-hero-carousel" role="button" data-bs-slide="next">
            <i class="fas fa-chevron-right"></i>
        </a>
    </div>
    <!-- End Banner Hero -->


    <!-- Start Categories of The Month -->
    <section class="container py-5">
        <div class="row text-center pt-3">
            <div class="col-lg-6 m-auto">
                <h1 class="h1">PILIH MOTOR FAVORIT ANDA</h1>
                <p>
                Klik pada tombol baca selengkapnya untuk melihat informasi detail Harga Motor, Simulasi Kredit Motor, dan Promo Motor Terbaru
                </p>
            </div>
        </div>
        <div class="row">
            <?php foreach($motors as $motor) : ?>
            <div class="col-12 col-md-4 p-5 mt-3">
                <a href="<?= site_url('landing/singleMotor/'. $motor->id) ?>">
                    <?php
                    if(!empty($motor->foto)) {
                        $url_foto = base_url('uploads/'. $motor->foto);
                    } else {
                        $url_foto = base_url('uploads/no-Image.jpg');
                    }
                    ?>
                    <img src="<?= $url_foto ?>" class="rounded-circle img-fluid border" style="height: 270px; width:100%">
                </a>
                <h5 class="text-center mt-3 mb-3"><?= $motor->nama ?></h5>
                <h6 class="text-center mt-3 mb-3"><?= rp_rupiah_decimal($motor->harga) ?></h6>
                <p class="text-center"><a href="<?= site_url('landing/singleMotor/'. $motor->id) ?>" class="btn btn-success">Baca Selengkapnya</a></p>
            </div>
            <?php endforeach; ?>
        </div>
    </section>
    <!-- End Categories of The Month -->


    <!-- Start Featured Product -->
    <section class="bg-light">
        <div class="container py-5">
            <div class="row text-center py-3">
                <div class="col-lg-6 m-auto">
                    <h1 class="h1">Tentang Kami</h1>
                    <?= $profil->tentang_kami ?>
                </div>
            </div>
        </div>
    </section>
    <!-- End Featured Product -->
