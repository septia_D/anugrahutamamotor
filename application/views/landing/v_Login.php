<!-- Start Contact -->
<div class="container py-5">
    <div class="row py-5">
        <?php if (($this->session->flashdata('error-alert')) != "") : ?>
            <!--begin::Alert-->
            <div class="fv-row mb-8">
                <div class="alert alert-danger d-flex align-items-center p-5">
                    <!--begin::Wrapper-->
                    <div class="d-flex flex-column">
                        <!--begin::Content-->
                        <span><?= $this->session->flashdata('error-alert') ?></span>
                        <!--end::Content-->
                    </div>
                    <!--end::Wrapper-->
                </div>
            </div>
            <!--end::Alert-->
        <?php endif; ?>
        <form class="col-md-9 m-auto" action="<?= site_url("landing/loginPelanggan"); ?>" method="post" role="form">
            <div class="row">
                <div class="form-group col-md-12 mb-3">
                    <label for="inputname">Username</label>
                    <input type="text" class="form-control mt-1" id="username" name="username"  required>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-12 mb-3">
                    <label for="inputemail">Password</label>
                    <input type="password" class="form-control mt-1" id="password" name="password" required>
                </div>
            </div>
            <div class="row">
                <div class="col text-center mt-2 mb-5">
                    <button type="submit" class="btn btn-success btn-lg px-3">Login</button>
                </div>
            </div>
        </form>
        <!-- Register buttons -->
        <div class="text-center">
            <p>Belum Punya Akun? <a href="<?= site_url('landing/register') ?>">Register</a></p>
        </div>
    </div>
</div>
<!-- End Contact -->
