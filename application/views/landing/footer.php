 <!-- Start Footer -->
 <footer class="bg-dark" id="tempaltemo_footer">
        <div class="container">
            <div class="row">
                <div class="col-md-4 pt-5">
                    <h2 class="h2 text-success border-bottom pb-3 border-light logo">Anugrah Utama Motor Purbalingga</h2>
                    <ul class="list-unstyled text-light footer-link-list">
                        <li>
                            <span class="fa-fw">WA</span>
                            <a class="navbar-sm-brand text-light text-decoration-none" href="https://api.whatsapp.com/send?phone=<?= !empty($profil) ? $profil->whatsapp : '' ?>"><?= !empty($profil) ? $profil->whatsapp : '-' ?></a>
                        </li>
                        <li>
                            <i class="fa fa-phone fa-fw"></i>
                            <a class="navbar-sm-brand text-light text-decoration-none" href="tel:<?= !empty($profil) ? $profil->no_telpon : '' ?>"><?= !empty($profil) ? $profil->no_telpon : '-' ?></a>
                        </li>
                        <li>
                            <i class="fa fa-envelope fa-fw"></i>
                            <a class="navbar-sm-brand text-light text-decoration-none" href="mailto:<?= !empty($profil) ? $profil->email : '-' ?>"><?= !empty($profil) ? $profil->email : '-' ?></a>
                        </li>
                    </ul>
                </div>

                <div class="col-md-4 pt-5">
                    <h2 class="h2 text-light border-bottom pb-3 border-light">Peta</h2>
                    <iframe 
                    width="300" 
                    height="170" 
                    frameborder="0" 
                    scrolling="no" 
                    marginheight="0" 
                    marginwidth="0" 
                    src="https://maps.google.com/maps?q=<?= $profil->langitude ?>,<?= $profil->longitude ?>&hl=es&z=14&amp;output=embed"
                    >
                    </iframe>
                </div>

                <div class="col-md-4 pt-5">
                    <h2 class="h2 text-light border-bottom pb-3 border-light">Jam Buka</h2>
                    <div class="table-responsive">
                        <table class="" width="100%">
                            <?php foreach($jamBuka as $jb) : ?>
                            <tr>
                                <td width="20%"><p class="text-light"><?= $jb->hari ?></p></td>
                                <td width="2%"><p class="text-light">:</p></td>
                                <td><p class="text-light"><?= $jb->buka ?> - <?= $jb->tutup ?></p></td>
                            </tr>
                            <?php endforeach; ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="w-100 bg-black py-3">
            <div class="container">
                <div class="row pt-2">
                    <div class="col-12">
                        <p class="text-left text-light">
                            Copyright &copy; 2023 Anugrah Utama Motor Purbalingga
                        </p>
                    </div>
                </div>
            </div>
        </div>

    </footer>
    <!-- End Footer -->

    <!-- Start Script -->
    <script src="<?= base_url('assets/template/landing/js/jquery-1.11.0.min.js') ?>"></script>
    <script src="<?= base_url('assets/template/landing/js/jquery-migrate-1.2.1.min.js') ?>"></script>
    <script src="<?= base_url('assets/template/landing/js/bootstrap.bundle.min.js') ?>"></script>
    <script src="<?= base_url('assets/template/landing/js/templatemo.js') ?>"></script>
    <script src="<?= base_url('assets/template/landing/js/custom.js') ?>"></script>
    <!-- End Script -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    <?php if ($this->session->flashdata("success") != '') : ?>
    <script>
        Swal.fire({
            icon: 'success',
            title: 'Berhasil',
            text: '<?= $this->session->flashdata("success") ?>'
        })
    </script>
    <?php endif; ?>

    <?php if ($this->session->flashdata("error") != '') : ?>
    <script>
        Swal.fire({
            icon: 'error',
            title: 'Informasi',
            text: '<?= $this->session->flashdata("error") ?>'
        })
    </script>
    <?php endif; ?>
</body>

</html>