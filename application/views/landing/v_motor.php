<!-- Start Content -->
<div class="container py-5">
    <div class="row">
        <div class="col-lg-3">
            <h1 class="h2 pb-4">Categories</h1>
            <form action="<?= site_url("landing/motor") ?>" method="get" class="border-0 p-0">
                <?php $filter = !empty($this->input->get("filter")) ? $this->input->get("filter") : '' ; ?>
                <div class="form-check">
                    <input name="filter" class="form-check-input position-static filter" type="radio" id="bebek-filter" value="Bebek" aria-label="..." <?= ($filter == "Bebek") ? "checked" : ""  ?>>
                    <label for="bebek-filter">Bebek</label>
                </div>
                <div class="form-check">
                    <input name="filter" class="form-check-input position-static filter" type="radio" id="matic-filter" value="Matic" aria-label="..." <?= ($filter == "Matic") ? "checked" : ""  ?>>
                    <label for="matic-filter">Matic</label>
                </div>
                <div class="form-check">
                    <input name="filter" class="form-check-input position-static filter" type="radio" id="sport-filter" value="Sport" aria-label="..." <?= ($filter == "Sport") ? "checked" : ""  ?>>
                    <label for="sport-filter">Sport</label>
                </div>
                <br>
                <button type="submit" class="input-group-text bg-success text-light">
                    <i class="fa fa-fw fa-filter text-white"></i>
                </button>
            </form>
        </div>

        <div class="col-lg-9">
            <div class="row">
                <?php foreach($motors as $motor) : ?>
                    <div class="col-md-4">
                        <div class="card mb-4 product-wap rounded-0">
                            <div class="card rounded-0">
                                <?php
                                if(!empty($motor->foto)) {
                                    $url_foto = base_url('uploads/'. $motor->foto);
                                } else {
                                    $url_foto = base_url('uploads/no-Image.jpg');
                                }
                                ?>
                                <img class="card-img rounded-0 img-fluid" src="<?= $url_foto ?>" style="height: 270px; width:100%">
                                <div class="card-img-overlay rounded-0 product-overlay d-flex align-items-center justify-content-center">
                                    <ul class="list-unstyled">
                                        <li><a class="btn btn-success text-white mt-2" href="<?= site_url('landing/singleMotor/'. $motor->id) ?>"><i class="fas fa-cart-plus"></i> Beli</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="card-body">
                                <a href="shop-single.html" class="h3 text-decoration-none"><?= $motor->nama ?></a>
                                <ul class="w-100 list-unstyled d-flex justify-content-between mb-0">
                                    <li><?= $motor->tipe ?></li>
                                </ul>
                                <p class="text-center mb-0"><?= rp_rupiah_decimal($motor->harga) ?></p>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>

    </div>
</div>
<!-- End Content -->