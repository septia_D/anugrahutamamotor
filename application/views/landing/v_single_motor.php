 <!-- Open Content -->
 <section class="bg-light">
        <div class="container pb-5">
            <div class="row">
                <div class="col-lg-5 mt-5">
                    <div class="card mb-3">
                        <?php
                        if(!empty($motor->foto)) {
                            $url_foto = base_url('uploads/'. $motor->foto);
                        } else {
                            $url_foto = base_url('uploads/no-Image.jpg');
                        }
                        ?>
                        <img class="card-img img-fluid" src="<?= $url_foto ?>" alt="Card image cap" id="product-detail">
                    </div>
                </div>
                <!-- col end -->
                <div class="col-lg-7 mt-5">
                    <div class="card">
                        <div class="card-body">
                            <h1 class="h2"><?= $motor->nama ?></h1>
                            <ul class="list-inline">
                                <li class="list-inline-item">
                                    <h6>Tipe:</h6>
                                </li>
                                <li class="list-inline-item">
                                    <p class="text-muted"><strong><?= $motor->tipe ?></strong></p>
                                </li>
                            </ul>
                            <p class="h3 py-2"><?= rp_rupiah_decimal($motor->harga) ?></p>
                            
                            <h6>Specification:</h6>
                            <div>
                                <?= $motor->spesifikasi ?>
                            </div>

                            <form action="#" method="GET">
                                <input type="hidden" name="product-title" value="Activewear">
                                <div class="row pb-3">
                                    <div class="col d-grid">
                                        <a href="https://api.whatsapp.com/send?phone=<?= !empty($profil) ? $profil->whatsapp : '' ?>&text=<?= $motor->nama ?>" class="btn btn-success btn-lg" name="submit" value="buy">WhatsApp</a>
                                    </div>
                                    <div class="col d-grid">
                                        <a href="tel:<?= !empty($profil) ? $profil->no_telpon : '-' ?>" class="btn btn-success btn-lg" name="submit" value="addtocard">Telpon</a>
                                    </div>
                                </div>
                            </form>
                            <div class="row">
                                <div class="col d-grid">
                                    <a href="<?= site_url("landing/beliMotor/". $motor->id) ?>" class="btn btn-primary btn-lg">Beli Langsung</a>
                                </div>  
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Close Content -->