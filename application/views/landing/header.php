<!DOCTYPE html>
<html lang="en">

<head>
    <title>Home Page</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- <link rel="apple-touch-icon" href="assets/img/apple-icon.png"> -->
    <!-- <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.ico"> -->

    <link rel="stylesheet" href="<?= base_url('assets/template/landing/css/bootstrap.min.css') ?>">
    <link rel="stylesheet" href="<?= base_url('assets/template/landing/css/templatemo.css') ?>">
    <link rel="stylesheet" href="<?= base_url('assets/template/landing/css/custom.css') ?>">

    <!-- Load fonts style after rendering the layout styles -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;200;300;400;500;700;900&display=swap">
    <link rel="stylesheet" href="<?= base_url('assets/template/landing/css/fontawesome.min.css') ?>">
<!--
    
TemplateMo 559 Zay Shop

https://templatemo.com/tm-559-zay-shop

-->
</head>

<body>
    <!-- Start Top Nav -->
    <nav class="navbar navbar-expand-lg bg-dark navbar-light d-none d-lg-block" id="templatemo_nav_top">
        <div class="container text-light">
            <div class="w-100 d-flex justify-content-between">
                <div>
                    <i class="fa fa-envelope mx-2"></i>
                    <a class="navbar-sm-brand text-light text-decoration-none" href="mailto:<?= !empty($profil) ? $profil->email : '-' ?>"><?= !empty($profil->email) ? $profil->email : '-' ?></a>
                    | <i class="fa fa-phone mx-2"></i>
                    <a class="navbar-sm-brand text-light text-decoration-none" href="tel:<?= !empty($profil) ? $profil->no_telpon : '' ?>"><?= !empty($profil) ? $profil->no_telpon : '-' ?></a>
                    | <span class="fa-fw">WA</span>
                    <a class="navbar-sm-brand text-light text-decoration-none" href="https://api.whatsapp.com/send?phone=<?= !empty($profil) ? $profil->whatsapp : '' ?>"><?= !empty($profil) ? $profil->whatsapp : '-' ?></a>
                </div>
            </div>
        </div>
    </nav>
    <!-- Close Top Nav -->


    <!-- Header -->
    <nav class="navbar navbar-expand-lg navbar-light shadow">
        <div class="container d-flex justify-content-between align-items-center">

            <a class="navbar-brand text-success logo h1 align-self-center" href="<?= site_url('/') ?>">
                <img src="<?= base_url('uploads/logo.jpeg') ?>" alt="LOGO" width="30%">
            </a>

            <button class="navbar-toggler border-0" type="button" data-bs-toggle="collapse" data-bs-target="#templatemo_main_nav" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="align-self-center collapse navbar-collapse flex-fill  d-lg-flex justify-content-lg-between" id="templatemo_main_nav">
                <div class="flex-fill">
                    <ul class="nav navbar-nav d-flex justify-content-between mx-lg-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="<?= site_url('home') ?>">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="<?= site_url('landing/motor') ?>">Motor</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="<?= site_url("landing/about") ?>">Tentang Kami</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="<?= site_url('landing/contact'); ?>">Kontak</a>
                        </li>
                    </ul>
                </div>
                <div class="navbar align-self-center d-flex">
                    <div class="d-lg-none flex-sm-fill mt-3 mb-4 col-7 col-sm-auto pr-3">
                        <div class="input-group">
                            <input type="text" class="form-control" id="inputMobileSearch" placeholder="Search ...">
                            <div class="input-group-text">
                                <i class="fa fa-fw fa-search"></i>
                            </div>
                        </div>
                    </div>
                    <a class="nav-icon d-none d-lg-inline" href="#" data-bs-toggle="modal" data-bs-target="#templatemo_search">
                        <i class="fa fa-fw fa-search text-dark mr-2"></i>
                    </a>
                    <?php if($this->session->status == "user_pelanggan") { ?>
                        <div class="btn-group">
                            <div class="btn-group">
                                <a href="<?= site_url("landing/detailPelanggan") ?>" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                    User
                                    <i class="fa fa-fw fa-user text-dark mr-3"></i>
                                </a>
                            </div>
                            <a href="<?= site_url("landing/logoutPelanggan") ?>" class="btn btn-default text-danger">Logout</a>
                        </div>
                    <?php } else { ?>
                        <a class="nav-icon position-relative text-decoration-none" href="<?= site_url("landing/login"); ?>">
                            <i class="fa fa-fw fa-user text-dark mr-3"></i> Login
                        </a>
                    <?php } ?>
                </div>
            </div>

        </div>
    </nav>
    <!-- Close Header -->

    <!-- Modal -->
    <div class="modal fade bg-white" id="templatemo_search" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="w-100 pt-1 mb-5 text-right">
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="<?= site_url("landing/motor") ?>" method="get" class="modal-content modal-body border-0 p-0">
                <div class="input-group mb-2">
                    <input type="text" class="form-control" id="inputModalSearch" name="q" placeholder="Search ...">
                    <button type="submit" class="input-group-text bg-success text-light">
                        <i class="fa fa-fw fa-search text-white"></i>
                    </button>
                </div>
            </form>
        </div>
    </div>