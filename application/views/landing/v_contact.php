    <!-- Start Content Page -->
    <div class="container-fluid bg-light py-5">
        <div class="col-md-6 m-auto text-center">
            <h1 class="h1">Hubungi Kami</h1>
            <p>
                Kontak Info
                <ul class="list-unstyled text-dark footer-link-list">
                    <li>
                        <span class="fa-fw">WA</span>
                        <a class="navbar-sm-brand text-dark text-decoration-none" href="https://api.whatsapp.com/send?phone=6281318615150&text=Saya%20tertarik%20untuk%20membeli%20motor%20HONDA"><?= !empty($profil) ? $profil->whatsapp : '-' ?></a>
                    </li>
                    <li>
                        <i class="fa fa-phone fa-fw"></i>
                        <a class="navbar-sm-brand text-dark text-decoration-none" href="tel:<?= !empty($profil) ? $profil->no_telpon : '-' ?>"><?= !empty($profil) ? $profil->no_telpon : '-' ?></a>
                    </li>
                    <li>
                        <i class="fa fa-envelope fa-fw"></i>
                        <a class="navbar-sm-brand text-dark text-decoration-none" href="mailto:<?= !empty($profil) ? $profil->email : '-' ?>"><?= !empty($profil) ? $profil->email : '-' ?></a>
                    </li>
                </ul>
            </p>
        </div>
    </div>

    <!-- Start Contact -->
    <div class="container py-5">
        <div class="row py-5">
            <form class="col-md-9 m-auto" action="<?= site_url("landing/sendPesan"); ?>" method="post" role="form">
                <div class="row">
                    <div class="form-group col-md-6 mb-3">
                        <label for="inputname">Nama</label>
                        <input type="text" class="form-control mt-1" id="name" name="name" placeholder="Nama" required>
                    </div>
                    <div class="form-group col-md-6 mb-3">
                        <label for="inputemail">Nomor Whatsapp</label>
                        <input type="text" class="form-control mt-1" id="email" name="no_whatsapp" placeholder="Nomor Whatsapp" required>
                    </div>
                </div>
                <div class="mb-3">
                    <label for="inputsubject">Judul</label>
                    <input type="text" class="form-control mt-1" id="subject" name="subject" placeholder="Judul" required>
                </div>
                <div class="mb-3">
                    <label for="inputmessage">Pesan</label>
                    <textarea class="form-control mt-1" id="message" name="message" placeholder="Pesan" rows="8" required></textarea>
                </div>
                <div class="row">
                    <div class="col text-end mt-2">
                        <button type="submit" class="btn btn-success btn-lg px-3">Kirim Pesan</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- End Contact -->