<style>
    .teks_besar {
        font-size: 70px;
    }

    /* membuat minimal tinggi satu elemen terutama contain di tab pane */
    .minimal_tinggi {
        min-height: 450px;
    }

    /* membuat background abu abu di tab pane bagian header */
    .bg_abu_abu {
        background-color: #ddd;
    }

    /* menyamakan lebar addon jadi 50 px */
    .input-group-addon {
        min-width: 50px;
    }

    /* pengganti formcontrol jika tidak berfungsi di kondisi tertentu */
    .lebar_peuh {
        width: 100%;
    }

    /* membuat border 1px tipis di tab pane  */
    .border_tipis {
        border: 1px solid #ddd;
    }

    /* meminimalkan margin  */
    .minimal_margin {
        margin: 0px;
    }

    /* menyempitkan tabel pembungkus form pasien */
    .minimal_padding>tbody>tr>td,
    .minimal_padding {
        padding: 3px;
    }

    /* PENTNG memberi tanda hijau di span */
    .terisi {
        background-color: #00a65a !important;
        color: #ddd !important;
    }

    /* PENTNG memberi tanda merah di span */
    .kosong {
        background-color: #d73925 !important;
        color: #ddd !important;
    }
</style>
<section class="bg-light">
    <div class="container py-5">
        <div class="row py-3">
            <div class="col-lg-12 m-auto">    
                <form action="<?= site_url("landing/checkoutMotor") ?>" method="post">
                    <table class="table table-striped table-bordered minimal_margin minimal_padding">
                        <input type="hidden" name="user_id" value="<?= !empty($user) ? $user->id : '' ?>">
                        <input type="hidden" name="motor_id" value="<?= !empty($motor) ? $motor->id : '' ?>">
                        <input type="hidden" name="harga" value="<?= !empty($motor) ? $motor->harga : '' ?>">
                        <tr>
                            <th width="15%">Nama</th>
                            <td width="1%">:</td>
                            <td>
                                <?= !empty($user) ? $user->nama : '' ?>
                            </td>
                        </tr>
                        <tr>
                            <th>No. HP</th>
                            <td>:</td>
                            <td>
                                <?= !empty($user) ? $user->no_hp : '' ?>
                            </td>
                        </tr>
                        <tr>
                            <th>Alamat</th>
                            <td>:</td>
                            <td>
                                <?= !empty($user) ? $user->alamat : '' ?>
                            </td>
                        </tr>
                        <tr>
                            <th>Motor</th>
                            <td>:</td>
                            <td>
                                <?= !empty($motor) ? $motor->nama : '' ?>
                            </td>
                        </tr>
                        <tr>
                            <th>Harga</th>
                            <td>:</td>
                            <td>
                                <?= !empty($motor) ? rp_rupiah_decimal($motor->harga) : '' ?>
                            </td>
                        </tr>
                        <tr>
                            <th></th>
                            <td></td>
                            <td>
                                <button type="submit" class="btn btn-primary">Checkout</button>
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>
    </div>
</section>