<!--begin::Content-->
<div id="kt_app_content" class="app-content flex-column-fluid pt-5">
    <!--begin::Content container-->
    <div id="kt_app_content_container" class="app-container container-fluid">
        <!--begin::Row-->
        <div class="row g-5 mb-5 mb-xl-10">
            <!--begin::Col-->
            <div class="col-lg-12">
                <!--begin::Engage widget 10-->
                <div class="card card-flush">
                    <!--begin::Body-->
                    <div class="card-body d-flex flex-column pb-0" style="background-position: 100% 50%; background-image:url('<?= base_url('assets/template/media/stock/900x600/42.png') ?>')">
                        <!--begin::Wrapper-->
                        <div class="mb-10">
                            <!--begin::Title-->
                            <div class="fs-2hx fw-bold text-gray-800 text-center mb-13">
                            <span class="me-2">Selamat Datang
                            <br />
                            <span class="position-relative d-inline-block text-danger">
                                <!--begin::Separator-->
                                <span class="position-absolute opacity-15 bottom-0 start-0 border-4 border-danger border-bottom w-100"></span>
                                <!--end::Separator-->
                            </span></span></div>
                            <!--end::Title-->
                        </div>
                        <!--begin::Wrapper-->
                        <!--begin::Illustration-->
                        <img class="mx-auto h-250px h-lg-400px theme-light-show" src="<?= base_url('assets/images/sample-motor.jpeg') ?>" alt="" />
                        <!--end::Illustration-->
                    </div>
                    <!--end::Body-->
                </div>
                <!--end::Engage widget 10-->
            </div>
            <!--end::Col-->
        </div>
    </div>
</div>
<?php $this->load->view("layout/extends-js") ?>
