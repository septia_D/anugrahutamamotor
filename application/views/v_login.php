
<!DOCTYPE html>
<html lang="en">
	<!--begin::Head-->
	<head><base href="../../../"/>
        <title>Log In</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--begin::Fonts(mandatory for all pages)-->
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Inter:300,400,500,600,700" />
		<!--end::Fonts-->
		<!--begin::Global Stylesheets Bundle(mandatory for all pages)-->
		<link href="<?= base_url('assets/template/plugins/global/plugins.bundle.css') ?>" rel="stylesheet" type="text/css" />
		<link href="<?= base_url('assets/template/css/style.bundle.css') ?>" rel="stylesheet" type="text/css" />
		<!--end::Global Stylesheets Bundle-->
	</head>
	<!--end::Head-->
	<!--begin::Body-->
	<body id="kt_body" class="app-blank app-blank bgi-size-cover bgi-position-center bgi-no-repeat">
		<!--begin::Root-->
		<div class="d-flex flex-column flex-root" id="kt_app_root">
			<!--begin::Page bg image-->
			<style>body { background-image: url('<?= base_url('assets/template/media/auth/bg10.jpeg') ?>'); }</style>
			<!--end::Page bg image-->
			<!--begin::Authentication - Sign-in -->
			<div class="d-flex flex-column flex-lg-row flex-column-fluid">
				<!--begin::Aside-->
				<div class="d-flex flex-lg-row-fluid">
					<!--begin::Content-->
					<div class="d-flex flex-column flex-center pb-0 pb-lg-10 w-100">
						<!--begin::Image-->
						<img class="theme-light-show mx-auto mw-200 w-250px w-lg-450px mb-lg-20" src="<?= base_url('assets/images/sample-motor.jpeg') ?>" alt="" />
						<!--end::Image-->
						<!--begin::Title-->
						<h1 class="text-gray-800 fs-2qx fw-bold text-center mb-7"></h1>
						<!--end::Title-->
					</div>
					<!--end::Content-->
				</div>
				<!--begin::Aside-->
				<!--begin::Body-->
				<div class="d-flex flex-column-fluid flex-lg-row-auto justify-content-center justify-content-lg-end p-12">
					<!--begin::Wrapper-->
					<div class="bg-body d-flex flex-center rounded-4 w-md-600px p-10">
						<!--begin::Content-->
						<div class="w-md-400px w-300px">
							<!--begin::Form-->
							<form class="form w-100" novalidate="novalidate" id="form_login" data-kt-redirect-url="<?= site_url("login/authentication") ?>" action="<?= site_url("login/authentication") ?>" method="POST">
								<!--begin::Heading-->
								<div class="text-center mb-11">
									<!--begin::Title-->
									<h1 class="text-dark fw-bolder mb-3">Log In</h1>
									<!--end::Title-->
								</div>
								<?php if (($this->session->flashdata('error-alert')) != "") : ?>
								<!--begin::Alert-->
								<div class="fv-row mb-8">
									<div class="alert alert-danger d-flex align-items-center p-5">
										<!--begin::Wrapper-->
										<div class="d-flex flex-column">
											<!--begin::Content-->
											<span><?= $this->session->flashdata('error-alert') ?></span>
											<!--end::Content-->
										</div>
										<!--end::Wrapper-->
									</div>
								</div>
								<!--end::Alert-->
								<?php endif; ?>
								<!--begin::Input group=-->
								<div class="fv-row mb-8">
									<!--begin::Email-->
									<input type="text" placeholder="Username" name="username" autocomplete="off" class="form-control bg-transparent" value="<?= $username ?>"/>
									<!--end::Email-->
								</div>
								<!--end::Input group=-->
								<div class="fv-row mb-8">
									<!--begin::Password-->
									<input type="password" placeholder="Password" name="password" autocomplete="off" class="form-control bg-transparent" />
									<!--end::Password-->
								</div>
								<!--end::Input group=-->
								<!--begin::Submit button-->
								<div class="d-grid mb-10">
									<button type="submit" id="kt_sign_in_submit" class="btn btn-primary">
										<!--begin::Indicator label-->
										<span class="indicator-label">Log In</span>
										<!--end::Indicator label-->
										<!--begin::Indicator progress-->
										<span class="indicator-progress">Please wait...
										<span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
										<!--end::Indicator progress-->
									</button>
								</div>
								<!--end::Submit button-->
							</form>
							<!--end::Form-->
						</div>
						<!--end::Content-->
					</div>
					<!--end::Wrapper-->
				</div>
				<!--end::Body-->
			</div>
			<!--end::Authentication - Sign-in-->
		</div>
		<!--begin::Javascript-->
		<script>var hostUrl = "assets/";</script>
		<!--begin::Global Javascript Bundle(mandatory for all pages)-->
		<script src="<?= base_url('assets/template/plugins/global/plugins.bundle.js') ?>"></script>
		<script src="<?= base_url('assets/template/js/scripts.bundle.js') ?>"></script>
		<!--end::Global Javascript Bundle-->
		<!--begin::Custom Javascript(used for this page only)-->
		<script src="<?= base_url('assets/template/js/custom/authentication/sign-in/general.js') ?>"></script>
		<!--end::Custom Javascript-->
		<!--end::Javascript-->
		<script>
			var form;
			var submitButton;
    		var validator;

			form = document.querySelector('#form_login');
            submitButton = document.querySelector('#kt_sign_in_submit');

			// Handle form
			var handleValidation = function(e) {
				// Init form validation rules. For more info check the FormValidation plugin's official documentation:https://formvalidation.io/
				validator = FormValidation.formValidation(
					form,
					{
						fields: {					
							'username': {
								validators: {
									notEmpty: {
										message: 'username harus diisi'
									}
								}
							},
							'password': {
								validators: {
									notEmpty: {
										message: 'Password harus diisi'
									}
								}
							} 
						},
						plugins: {
							trigger: new FormValidation.plugins.Trigger(),
							bootstrap: new FormValidation.plugins.Bootstrap5({
								rowSelector: '.fv-row',
								eleInvalidClass: '',  // comment to enable invalid state icons
								eleValidClass: '' // comment to enable valid state icons
							})
						}
					}
				);	
			}

			var handleSubmit = function(e) {
				// Handle form submit
				submitButton.addEventListener('click', function (e) {
					// Prevent button default action
					e.preventDefault();

					// Validate form
					validator.validate().then(function (status) {
						if (status == 'Valid') {
							// Show loading indication
							submitButton.setAttribute('data-kt-indicator', 'on');

							// Disable button to avoid multiple click 
							submitButton.disabled = true;  
															
							form.submit();  						
						} else {
							// Show error popup. For more info check the plugin's official documentation: https://sweetalert2.github.io/
							Swal.fire({
								text: "Maaf, sepertinya ada beberapa kesalahan yang terdeteksi, silakan coba lagi.",
								icon: "error",
								buttonsStyling: false,
								confirmButtonText: "Ya, lanjutkan",
								customClass: {
									confirmButton: "btn btn-primary"
								}
							});
						}
					});
				});
			}

			handleValidation();
			handleSubmit();
		</script>
		<?php if($this->session->flashdata("error") != "") : ?>
		<script>
			Swal.fire({
				text: "<?= $this->session->flashdata("error") ?>",
				icon: "error",
				buttonsStyling: false,
				confirmButtonText: "Ok",
				customClass: {
					confirmButton: "btn btn-primary"
				}
			})
		</script>
		<?php endif; ?>
	</body>
	<!--end::Body-->
</html>