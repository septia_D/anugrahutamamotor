<!--begin::Content wrapper-->
<div class="d-flex flex-column flex-column-fluid">
    <!--begin::Toolbar-->
    <div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
        <!--begin::Toolbar container-->
        <div id="kt_app_toolbar_container" class="app-container">
            <!--begin::Page title-->
            <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
                <!--begin::Title-->
                <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">Motor Edit Form</h1>
                <!--end::Title-->
                <!--begin::Breadcrumb-->
                <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                    <!--begin::Item-->
                    <li class="breadcrumb-item text-muted">
                        <a href="#" class="text-muted text-hover-primary">Home</a>
                    </li>
                    <!--end::Item-->
                    <!--begin::Item-->
                    <li class="breadcrumb-item">
                        <span class="bullet bg-gray-400 w-5px h-2px"></span>
                    </li>
                    <!--end::Item-->
                    <!--begin::Item-->
                    <li class="breadcrumb-item text-muted">Motor</li>
                    <!--end::Item-->
                </ul>
                <!--end::Breadcrumb-->
            </div>
            <!--end::Page title-->
        </div>
        <!--end::Toolbar container-->
    </div>
    <!--end::Toolbar-->
    <!--begin::Form-->
    <form id="motor_edit_form" class="form" action="<?= site_url('motor/update') ?>" method="post" enctype="multipart/form-data">
        <input type="hidden" name="id" value="<?= $id ?>">
        <!--end::Input-->
        <!--begin::Content-->
        <div id="kt_app_content" class="app-content flex-column-fluid">
            <!--begin::Content container-->
            <div id="kt_app_content_container" class="app-container">
                <!--begin::form hps-->
                <div class="row g-7 mb-5">
                    <!--begin::Content-->
                    <div class="col-xl-12">
                        <!--begin::Contacts-->
                        <div class="card card-flush h-lg-100" id="kt_hps_main">
                            <!--begin::Card body-->
                            <div class="card-body pt-5">
                                <!--begin::Input group-->
                                <div class="row mb-5">
                                    <!--begin::Label-->
                                    <label class="fs-6 fw-semibold form-label mt-3">
                                        <span class="required">Nama</span>
                                    </label>
                                    <!--begin::Input-->
                                    <input type="text" name="nama" class="form-control" id="nama" value="<?= $nama ?>" autocomplete="off"/>
                                    <!--end::Input-->
                                </div>
                                
                                <!--begin::Input group-->
                                <div class="row mb-5">
                                    <!--begin::Label-->
                                    <label class="fs-6 fw-semibold form-label mt-3">
                                        <span class="required">Tipe</span>
                                    </label>
                                    <!--begin::Input-->
                                    <select name="tipe" class="form-select" data-control="select2" data-placeholder="Pilih" required>
                                        <option value="Bebek" <?= ($tipe == "Bebek") ? "selected" : "" ?>>Bebek</option>
                                        <option value="Matic" <?= ($tipe == "Matic") ? "selected" : "" ?>>Matic</option>
                                        <option value="Sport" <?= ($tipe == "Sport") ? "selected" : "" ?>>Sport</option>
                                    </select>
                                    <!--end::Input-->
                                </div>

                                <!--begin::Input group-->
                                <div class="row mb-5">
                                    <!--begin::Label-->
                                    <label class="fs-6 fw-semibold form-label mt-3">
                                        <span class="required">Harga</span>
                                    </label>
                                    <!--begin::Input-->
                                    <input type="text" class="form-control" placeholder="" name="harga" id="harga" value="<?= $harga ?>" required/>
                                    <!--end::Input-->
                                </div>
                                
                                <!--begin::Input group-->
                                <div class="row mb-5">
                                    <!--begin::Label-->
                                    <label class="fs-6 fw-semibold form-label mt-3">
                                        <span class="required">Spesifikasi</span>
                                    </label>
                                    <!--begin::Input-->
                                    <div class="col-md-12">
                                        <textarea name="spesifikasi" id="kt_docs_ckeditor_classic"><?= $spesifikasi ?></textarea>
                                    </div>
                                    <!--end::Input-->
                                </div>
                                
                                <!--end::Input group-->
                                <div class="row mb-5">
                                    <!--begin::Label-->
                                    <label class="fs-6 fw-semibold mb-2">Foto</label>
                                    <!--end::Label-->
                                    <input type="file" class="form-control" placeholder="" name="document" accept="image/png, image/gif, image/jpeg, webp" required/>
                                </div>
                                
                            </div>
                            <!--end::Card body-->
                        </div>
                        <!--end::Contacts-->
                    </div>
                    <!--end::Content-->

                    
                </div>

                <div class="d-flex justify-content-end">
                    <a href="<?= site_url('motor') ?>"  class="btn btn-light me-3">Kembali</a>
                    
                    <button type="submit" id="kt_add_hps_submit" class="btn btn-primary me-5">
                        <span class="indicator-label">Simpan</span>
                        <span class="indicator-progress">Please wait...
                        <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                    </button>
                </div>
            </div>
        </div>
    </form>
    <!--end::Form-->
</div>

<!--begin:Javascript -->
<?php $this->load->view("layout/extends-js") ?>
<!--end::Javascript-->
<!--CKEditor Build Bundles:: Only include the relevant bundles accordingly-->
<script src="<?= base_url('assets/template/plugins/custom/ckeditor/ckeditor-classic.bundle.js') ?>"></script>
<script>
    ClassicEditor
    .create(document.querySelector('#kt_docs_ckeditor_classic'))
    .then(editor => {
        console.log(editor);
    })
    .catch(error => {
        console.error(error);
    });

    $(function(){
		$("#harga").keyup(function(e){
			$(this).val(format($(this).val()));
		});
    	});
		var format = function(num){
		var str = num.toString().replace("", ""), parts = false, output = [], i = 1, formatted = null;
		if(str.indexOf(",") > 0) {
			parts = str.split(",");
			str = parts[0];
		}
		str = str.split("").reverse();
		for(var j = 0, len = str.length; j < len; j++) {
			if(str[j] != ".") {
			output.push(str[j]);
			if(i%3 == 0 && j < (len - 1)) {
				output.push(".");
			}
			i++;
			}
		}

		formatted = output.reverse().join("");
			return("" + formatted + ((parts) ? "," + parts[1].substr(0, 2) : ""));
    };

</script>
