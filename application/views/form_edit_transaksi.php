<!--begin::Content wrapper-->
<div class="d-flex flex-column flex-column-fluid">
    <!--begin::Toolbar-->
    <div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
        <!--begin::Toolbar container-->
        <div id="kt_app_toolbar_container" class="app-container">
            <!--begin::Page title-->
            <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
                <!--begin::Title-->
                <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">Transaksi Edit Form</h1>
                <!--end::Title-->
                <!--begin::Breadcrumb-->
                <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                    <!--begin::Item-->
                    <li class="breadcrumb-item text-muted">
                        <a href="#" class="text-muted text-hover-primary">Home</a>
                    </li>
                    <!--end::Item-->
                    <!--begin::Item-->
                    <li class="breadcrumb-item">
                        <span class="bullet bg-gray-400 w-5px h-2px"></span>
                    </li>
                    <!--end::Item-->
                    <!--begin::Item-->
                    <li class="breadcrumb-item text-muted">Transaksi</li>
                    <!--end::Item-->
                </ul>
                <!--end::Breadcrumb-->
            </div>
            <!--end::Page title-->
        </div>
        <!--end::Toolbar container-->
    </div>
    <!--end::Toolbar-->
    <!--begin::Form-->
        <!--begin::Content-->
        <div id="kt_app_content" class="app-content flex-column-fluid">
            <!--begin::Content container-->
            <div id="kt_app_content_container" class="app-container">
                <!--begin::form hps-->
                <div class="row g-7 mb-5">
                    <!--begin::Content-->
                    <div class="col-xl-12">
                        <!--begin::Contacts-->
                        <div class="card card-flush h-lg-100" id="kt_hps_main">
                            <!--begin::Card body-->
                            <div class="card-body pt-5">
                                <div class="col-lg-12 m-auto">    
                                    <form action="<?= site_url("transaksi/updateStatusAction") ?>" method="post">
                                        <table class="table table-striped table-bordered minimal_margin minimal_padding">
                                            <input type="hidden" name="id" value="<?= $transaksi->id ?>">
                                            <tr>
                                                <th width="15%">Nama Pembeli</th>
                                                <td width="1%">:</td>
                                                <td>
                                                    <?php  
                                                        $user = $this->db->get_where("user", ["id" => $transaksi->user_id])->row();
                                                        echo $user->nama;
                                                    ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>No. HP</th>
                                                <td>:</td>
                                                <td>
                                                    <?= !empty($user) ? $user->no_hp : '' ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Alamat</th>
                                                <td>:</td>
                                                <td>
                                                    <?= !empty($user) ? $user->alamat : '' ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Motor</th>
                                                <td>:</td>
                                                <td>
                                                    <?php 
                                                        $motor = $this->db->get_where("motor", ["id" => $transaksi->motor_id])->row();
                                                        echo $motor->nama;
                                                    ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Harga</th>
                                                <td>:</td>
                                                <td>
                                                    <?= !empty($motor) ? rp_rupiah_decimal($motor->harga) : '' ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Status</th>
                                                <td>:</td>
                                                <td>
                                                    <select name="status" class="form-select" data-control="select2" data-placeholder="Pilih" required>
                                                        <option></option>
                                                        <option value="selesai" <?= ($transaksi->status == "selesai") ? "selected" : "" ?>>Selesai</option>
                                                        <option value="batal" <?= ($transaksi->status == "batal") ? "selected" : "" ?>>Batal</option>
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th></th>
                                                <td></td>
                                                <td>
                                                    <button type="submit" class="btn btn-primary">Simpan</button>
                                                </td>
                                            </tr>
                                        </table>
                                    </form>
                                </div>
                                
                            </div>
                            <!--end::Card body-->
                        </div>
                        <!--end::Contacts-->
                    </div>
                    <!--end::Content-->

                    
                </div>

                <div class="d-flex justify-content-end">
                    <a href="<?= site_url('transaksi') ?>"  class="btn btn-light me-3">Kembali</a>
                </div>
            </div>
        </div>
    </form>
    <!--end::Form-->
</div>

<!--begin:Javascript -->
<?php $this->load->view("layout/extends-js") ?>
<!--end::Javascript-->
<!--CKEditor Build Bundles:: Only include the relevant bundles accordingly-->
<script src="<?= base_url('assets/template/plugins/custom/ckeditor/ckeditor-classic.bundle.js') ?>"></script>
<script>
    ClassicEditor
    .create(document.querySelector('#kt_docs_ckeditor_classic'))
    .then(editor => {
        console.log(editor);
    })
    .catch(error => {
        console.error(error);
    });

    $(function(){
		$("#harga").keyup(function(e){
			$(this).val(format($(this).val()));
		});
    	});
		var format = function(num){
		var str = num.toString().replace("", ""), parts = false, output = [], i = 1, formatted = null;
		if(str.indexOf(",") > 0) {
			parts = str.split(",");
			str = parts[0];
		}
		str = str.split("").reverse();
		for(var j = 0, len = str.length; j < len; j++) {
			if(str[j] != ".") {
			output.push(str[j]);
			if(i%3 == 0 && j < (len - 1)) {
				output.push(".");
			}
			i++;
			}
		}

		formatted = output.reverse().join("");
			return("" + formatted + ((parts) ? "," + parts[1].substr(0, 2) : ""));
    };

</script>
